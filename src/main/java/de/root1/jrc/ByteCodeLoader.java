/*
 * Copyright (C) 2015 Alexander Christian <alex(at)root1.de>. All rights reserved.
 * 
 * This file is part of JavaRuntimeCompiler (JRC).
 *
 *   JRC is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   JRC is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with JRC.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.root1.jrc;

import java.security.SecureClassLoader;

/**
 * {@code ByteCodeLoader} can be used for easy loading of byte code already
 * present in memory.
 *
 * {@code InMemoryCompiler} can be used for compiling source code in a string
 * into byte code, which then can be loaded with {@code ByteCodeLoader}.
 *
 * @see InMemoryCompiler
 */
public class ByteCodeLoader extends SecureClassLoader {
    private final String className;
    private final byte[] byteCode;

    /**
     * Creates a new {@code ByteCodeLoader} ready to load a class with the
     * given name and the given byte code.
     *
     * @param className The name of the class
     * @param byteCode The byte code of the class
     */
    public ByteCodeLoader(String className, byte[] byteCode) {
        this.className = className;
        this.byteCode = byteCode;
    }

    @Override
    protected Class<?> findClass(String name) throws ClassNotFoundException {
        if (!name.equals(className)) {
            throw new ClassNotFoundException(name);
        }

        return defineClass(name, byteCode, 0, byteCode.length);
    }

    /**
     * Utility method for creating a new {@code ByteCodeLoader} and then
     * directly load the given byte code.
     *
     * @param className The name of the class
     * @param byteCode The byte code for the class
     * @throws ClassNotFoundException if the class can't be loaded
     * @return A {@see Class} object representing the class
     */
    public static Class<?> load(String className, byte[] byteCode) throws ClassNotFoundException {
        return new ByteCodeLoader(className, byteCode).loadClass(className);
    }
}
