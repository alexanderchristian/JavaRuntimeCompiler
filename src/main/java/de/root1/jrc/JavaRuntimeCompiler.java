/*
 * Copyright (C) 2015 Alexander Christian <alex(at)root1.de>. All rights reserved.
 * 
 * This file is part of JavaRuntimeCompiler (JRC).
 *
 *   JRC is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   JRC is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with JRC.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.root1.jrc;

import java.io.File;
import java.io.FileFilter;
import java.io.FilenameFilter;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import javax.tools.Diagnostic;
import javax.tools.DiagnosticCollector;
import javax.tools.JavaCompiler;
import javax.tools.JavaCompiler.CompilationTask;
import javax.tools.JavaFileObject;
import javax.tools.ToolProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class JavaRuntimeCompiler {
    
    private static final Logger log = LoggerFactory.getLogger(JavaRuntimeCompiler.class);
    
    static {
        JavaCompiler compiler = JavaRuntimeCompiler.getCompiler();
        if (compiler==null) {
            log.error("No Java Compiler found. Running with JRE instead of JDK? Terminating application.");
            System.exit(1);
        }
    }


    private boolean generateDebugInformation = true;

    private List<String> classPath = new ArrayList<>();

    public boolean isGenerateDebugInformation() {
        return generateDebugInformation;
    }

    public void setGenerateDebugInformation(boolean generateDebugInformation) {
        this.generateDebugInformation = generateDebugInformation;
    }

    public List<String> getClassPath() {
        return classPath;
    }

    public void setClassPath(List<String> classPath) {
        this.classPath = classPath;
    }
    
    public void addClassPath(File f) {
        
        if (f.isDirectory()) {
            File[] list = f.listFiles(new FileFilter() {

                @Override
                public boolean accept(File f) {
                    return f.isFile() && f.getName().endsWith(".jar");
                }
                
               
            });
            for (File jar : list) {
                log.debug("Adding JAR to classpath: "+jar.getAbsolutePath());
                classPath.add(jar.getAbsolutePath());
            }
        }
        classPath.add(f.getAbsolutePath());
    }

    public void addClassPath(URLClassLoader classLoader) {

        URLClassLoader ucl = (URLClassLoader) classLoader;
        URL[] urls = ucl.getURLs();

        for (URL url : urls) {
            try {
                
                
                // File f = new File(url.toURI()); no longer works for java>=9 (or 10)
                
                String uriString = url.toURI().toString();
                /*
                convert f.i. 
                    jar:file:/my/folder/ARCHIVE_logicplugin-1.0.0-SNAPSHOT-kadplugin.jar_1725192041399286589.deploytmp.jar!/
                to
                    /my/folder/ARCHIVE_logicplugin-1.0.0-SNAPSHOT-kadplugin.jar_1725192041399286589.deploytmp.jar
                */
                String localFileString = uriString.substring(0, uriString.length()-2).replace("jar:file:", "");
                File f = new File(localFileString);
                
                log.debug("Adding classpath from classloader: {}", f.getAbsolutePath());

                classPath.add(f.getAbsolutePath());
            } catch (URISyntaxException ex) {
                throw new RuntimeException("Problem reading classpath from URLClassLoader", ex);
            }
        }
    }

    public void setClassPath(URLClassLoader classLoader) {
        clearClassPath();
        addClassPath(classLoader);
    }

    public void clearClassPath() {
        classPath.clear();
    }

    /**
     * Compiles the class with the given name and source code in-memory. No
     * files are written to disk.
     *
     * @param className The name of the class
     * @param sourceCode The source code for the class with name
     * {@code className}
     * @throws CompileException if the compilation did not succeed
     * @return The resulting byte code from the compilation
     */
    public byte[] compileToMemory(String className, CharSequence sourceCode) throws CompileException {

        MemoryJavaFileObject file = new MemoryJavaFileObject(className, sourceCode);

        compile(file);

        return file.getByteCode();
    }

    /**
     * Compiles the given file
     *
     * @param classname canonical classname, e.g. foo.bar.MyClass
     * @param f source file to use
     * @param destinationFolder folder to compile to.
     * @throws CompileException if the compilation did not succeed
     * @return A container with all relevant information
     */
    public CompileResult compileToFile(String classname, File f, File destinationFolder) throws CompileException {

        DiskJavaFileObject file = new DiskJavaFileObject(classname, f, destinationFolder);

        
        Set<String> classnameSet = compile(file);
        
        CompileResult cr = new CompileResult();
        cr.setClassnames(classnameSet);
        cr.setFolder(destinationFolder);

        return cr;
    }

    
    

    /*
     * ############################################################
     */
    /**
     * Compiles the given file object
     *
     * @param file
     * @param diagnosticsCollector
     * @throws CompileException
     */
    private Set<String> compile(JRCJavaFileObject file) throws CompileException {
        DiagnosticCollector<JavaFileObject> diagnosticsCollector = new DiagnosticCollector<>();

        List<String> compileOptions = new ArrayList<>();
        if (generateDebugInformation) {
            compileOptions.add("-g");
            log.debug("Using generateDebugInformation");
        }

        if (classPath != null && !classPath.isEmpty()) {
            compileOptions.add("-classpath");
            StringBuilder classPathString = new StringBuilder();
            for (String cp : classPath) {
                classPathString.append(cp).append(File.pathSeparator);
            }
            compileOptions.add(classPathString.toString());
            log.debug("Using classpath: {}", classPathString.toString());
        }
        
        FileManagerWrapper fileManagerWrapper = new FileManagerWrapper(file);

        CompilationTask task = getCompiler().getTask(null, fileManagerWrapper, diagnosticsCollector, compileOptions, null, Arrays.asList(file));

        Boolean success = task.call();

        StringBuilder sb = new StringBuilder();
        // in case of compile error/problems, print diganostic information
        for (Diagnostic<?> diagnostic : diagnosticsCollector.getDiagnostics()) {

            sb.append("\nDiagnostic: \n" + diagnostic);

        }

        if (!success) {
            throw new CompileException("Could not compile. " + sb.toString(), diagnosticsCollector);
        }
        return fileManagerWrapper.getClassnameSet();
    }

    static JavaCompiler getCompiler() {
        return ToolProvider.getSystemJavaCompiler();
    }

}
