/*
 * Copyright (C) 2015 Alexander Christian <alex(at)root1.de>. All rights reserved.
 * 
 * This file is part of JavaRuntimeCompiler (JRC).
 *
 *   JRC is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   JRC is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with JRC.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.root1.jrc;

import java.io.File;
import java.net.URI;
import javax.tools.JavaFileObject;
import javax.tools.SimpleJavaFileObject;

/**
 *
 * @author achristian
 */
class JRCJavaFileObject extends SimpleJavaFileObject {

    private final String className;

    JRCJavaFileObject(String classname, URI uri, JavaFileObject.Kind kind) {
        super(uri, kind);
        this.className = classname;
    }

    public String getClassName() {
        return className;
    }

}
