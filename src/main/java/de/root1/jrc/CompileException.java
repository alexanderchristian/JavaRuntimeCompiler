/*
 * Copyright (C) 2015 Alexander Christian <alex(at)root1.de>. All rights reserved.
 * 
 * This file is part of JavaRuntimeCompiler (JRC).
 *
 *   JRC is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   JRC is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with JRC.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.root1.jrc;

import javax.tools.DiagnosticCollector;
import javax.tools.JavaFileObject;

/**
 *
 * @author achristian
 */
public class CompileException extends Exception {

    private final DiagnosticCollector<JavaFileObject> diagnosticsCollector;

    public CompileException(DiagnosticCollector<JavaFileObject> diagnosticsCollector) {
        super();
        this.diagnosticsCollector = diagnosticsCollector;
    }

    public CompileException(String message, Throwable cause, DiagnosticCollector<JavaFileObject> diagnosticsCollector) {
        super(message, cause);
        this.diagnosticsCollector = diagnosticsCollector;
    }

    public CompileException(Throwable cause, DiagnosticCollector<JavaFileObject> diagnosticsCollector) {
        super(cause);
        this.diagnosticsCollector = diagnosticsCollector;
    }

    CompileException(String string, DiagnosticCollector<JavaFileObject> diagnosticsCollector) {
        super(string);
        this.diagnosticsCollector = diagnosticsCollector;
    }

    public DiagnosticCollector<JavaFileObject> getDiagnosticsCollector() {
        return diagnosticsCollector;
    }

}
