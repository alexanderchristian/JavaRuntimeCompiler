/*
 * Copyright (C) 2015 Alexander Christian <alex(at)root1.de>. All rights reserved.
 *
 * This file is part of JavaRuntimeCompiler (JRC).
 *
 *   JRC is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   JRC is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with JRC.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.root1.jrc;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import javax.tools.JavaFileObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author achristian
 */
class DiskJavaFileObject extends JRCJavaFileObject {

    private Logger log = LoggerFactory.getLogger(getClass());

    private final File srcFile;
    private final File dstFile;
    private final File destinationFolder;

    DiskJavaFileObject(String classname, File f, File destinationFolder) {
        super(classname, f.toURI(), JavaFileObject.Kind.SOURCE);
        this.srcFile = f;

        this.destinationFolder = destinationFolder;
        destinationFolder = destinationFolder.getAbsoluteFile();

        if (destinationFolder==null) {
            throw new IllegalArgumentException("destinationFolder must not be null.");
        }


        if (!destinationFolder.exists()) {
            throw new IllegalArgumentException("destination folder must exist: "+destinationFolder.getAbsolutePath());
        }

        File basedir = destinationFolder;

        String[] split = getClassName().split("\\.");
        String clzName = split[split.length-1];
        String packagepath = "";
        if (split.length>1) {
            for (int i = 0; i < split.length-1; i++) {
                packagepath+=split[i]+File.separator;
            }
        }
        File packagedDir = new File(basedir, packagepath);
        packagedDir.mkdirs();

        this.dstFile = new File(packagedDir, clzName+ ".class");
    }

    @Override
    public CharSequence getCharContent(boolean ignoreEncodingErrors) {
        StringBuilder src = new StringBuilder();
        try {

            FileReader fr = new FileReader(srcFile);
            char[] buf = new char[512];
            int read = 0;
            while ((read = fr.read(buf)) != -1) {
                src.append(buf, 0, read);
            }
            fr.close();

        } catch (IOException ex) {
            ex.printStackTrace();
        }

//        System.out.println(">>>>>>>>>>> SRC");
//        System.out.println(src.toString());
//        System.out.println("SRC <<<<<<<<<<<");
        return src.toString();
    }

    @Override
    public OutputStream openOutputStream() throws IOException {
        return new FileOutputStream(dstFile);
    }

    public byte[] getByteCode() {

        if (dstFile.length() > Integer.MAX_VALUE) {
            throw new IllegalStateException("Classfile " + dstFile.getAbsolutePath() + " is too big to load into byte[]");
        }
        byte[] byteCode = new byte[(int) dstFile.length()];

        FileInputStream fis = null;
        try {
            fis = new FileInputStream(dstFile);

            DataInputStream dis = new DataInputStream(fis);

            dis.readFully(byteCode);

        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            try {
                fis.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            return byteCode;
        }
    }

    public File getClassFile() {
        return dstFile;
    }

    public DiskJavaFileObject cloneForAnonymous(int anonymousClassFileNumber) {
        return new DiskJavaFileObject(getClassName() + "$" + anonymousClassFileNumber, srcFile, destinationFolder);
    }

    @Override
    public String toString() {
        return "DiskJavaFileObject{" + "srcFile=" + srcFile + ", dstFile=" + dstFile + '}';
    }





}
