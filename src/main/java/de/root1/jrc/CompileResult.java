/*
 * Copyright (C) 2015 Alexander Christian <alex(at)root1.de>. All rights reserved.
 *
 * This file is part of JavaRuntimeCompiler (JRC).
 *
 *   JRC is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   JRC is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with JRC.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.root1.jrc;

import java.io.File;
import java.util.Set;

/**
 *
 * @author achristian
 */
public class CompileResult {
    
    private Set<String> classnames;
    private File folder;

    void setClassnames(Set<String> classnames) {
        this.classnames = classnames;
    }

    public boolean containsClass(String clazzname) {
        return classnames.contains(clazzname);
    }

    /**
     * Returns a set of class names which where processed by compiler
     * @return 
     */
    public Set<String> getClassnames() {
        return classnames;
    }

    void setFolder(File folder) {
        this.folder = folder;
    }
    
    /**
     * Returns a file object referencing the .class file for given classname
     * @param classname
     * @return 
     */
    public File getFileFor(String classname) {
        if (!containsClass(classname)) {
            throw new IllegalArgumentException("Class '"+classname+"' now known by this compile result.");
        }
        
        String classfile = classname.replace(".", File.separator).concat(".class");
        return new File(folder, classfile);
    }
    
}
