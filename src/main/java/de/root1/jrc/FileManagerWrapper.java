/*
 * Copyright (C) 2015 Alexander Christian <alex(at)root1.de>. All rights reserved.
 * 
 * This file is part of JavaRuntimeCompiler (JRC).
 *
 *   JRC is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   JRC is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with JRC.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.root1.jrc;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import javax.tools.FileObject;
import javax.tools.ForwardingJavaFileManager;
import javax.tools.JavaFileManager;
import javax.tools.JavaFileObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author achristian
 */
class FileManagerWrapper extends ForwardingJavaFileManager {

    private Logger log = LoggerFactory.getLogger(getClass());
    
    /**
     * Set with classnames that where compiled (mainfile as well as anonymous classes)
     */
    private final Set<String> classnameSet = new HashSet<>();

    private final JRCJavaFileObject file;

    public FileManagerWrapper(JRCJavaFileObject file) {
        super(JavaRuntimeCompiler.getCompiler().getStandardFileManager(null, null, null));
        this.file = file;
        log.debug("wrapper for [{}]", file);
    }

    @Override
    public JavaFileObject getJavaFileForOutput(JavaFileManager.Location location, String className, JavaFileObject.Kind kind, FileObject sibling) throws IOException {

        int anonymousClassNumber = -1;
        if (className.contains("$")) {
            String[] split = className.split("\\$");
            anonymousClassNumber = Integer.parseInt(split[1]);
        }

        classnameSet.add(className);
        
        if (anonymousClassNumber!=-1 && file instanceof DiskJavaFileObject) {
            DiskJavaFileObject djfo = (DiskJavaFileObject) file;
            DiskJavaFileObject anonymousFile = djfo.cloneForAnonymous(anonymousClassNumber);
            log.debug("anonymous outputfile: [{}]", anonymousFile);
            return anonymousFile;
        }
        
        log.debug("outputfile: [{}]", file);
        
        return file;
    }

    public Set<String> getClassnameSet() {
        return classnameSet;
    }

    @Override
    public ClassLoader getClassLoader(Location location) {
        return super.getClassLoader(location);
    }

}
